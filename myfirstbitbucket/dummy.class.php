<?php
/**
 * 
 * @author fausto
 * commento successivo alla creazione per capire il funzionamento
 *
 *
 */
	class dummy{
		
		private $attr1;
		private $attr2;
		
		public function __construct(){
			$this->attr1 = null;
			$this->attr2 = null;
		}
		
		/**
		 * imposta il valore dell'attributo attr1 della classe 
		 * @param mixed $value
		 */
		public function set1( $value ) {
			$this->attr1 = $value;
		} // set1

		/**
		 * restituisce il valore dell'attributo attr1 della classe
		 */
		public function get1() {
			return $this->attr1;
		} // get1
		
		
		
	} // class